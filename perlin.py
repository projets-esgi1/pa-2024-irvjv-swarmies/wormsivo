import math 
import random

 
def interpolate( a0: float, a1: float, w: float):
    """
    Function to interpolate smoothly between a0 and a1
    Weight w should be in the range [0.0, 1.0]
    """
    #return (a1 - a0) * w + a0
    #return (a1 - a0) * (3.0 - w * 2.0) * w * w + a0
    return (a1 - a0) * ((w * (w * 6.0 - 15.0) + 10.0) * w * w * w) + a0

seed1 = random.randint(0, 1<<32)
seed2= random.randint(0, 1<<32)
seed3= random.randint(0, 1<<32)
# seed1 = 3284157443
# seed2= 1911520717
# seed3= 2048419325

def randomGradient( ix: int, iy: int)->tuple[float, float]:
    # No precomputed gradients mean this works for 
    # any number of grid coordinates
    w = 32
    s = w // 2 # rotation width
    a = ix
    b = iy
    #a *= 3284157443
    a *= seed1
    b ^= a << s | a >> w-s
    #b *= 1911520717
    b *= seed2
    a ^= b << s | b >> w-s
    #a *= 2048419325
    a *= seed3
    random_: float = a * (3.14159265 / (1 << 31)) # in [0, 2*Pi]

    return math.cos(random_), math.sin(random_)


def dotGridGradient( ix: int, iy: int, x: float, y: float):
    """
    Computes the dot product of the distance and gradient vectors.
    """
 
    # Precomputed (or otherwise) gradient vectors at each grid node
    #extern float Gradient[IYMAX][IXMAX][2]
    gradient = randomGradient(ix, iy)
 
    # Compute the distance vector
    dx: float = x - ix
    dy: float = y - iy
 
    # Compute the dot-product
    return (dx*gradient[0] + dy*gradient[1])
 
def perlin( x: float, y: float):
    """Compute Perlin noise at coordinates x, y"""

    # Determine grid cell coordinates
    x0: int = math.floor(x)
    x1: int = x0 + 1
    y0:int = math.floor(y)
    y1: int = y0 + 1
 
    # Determine interpolation weights
    # Could also use higher order polynomial/s-curve here
    sx: float = x - x0
    sy: float = y - y0
 
    # Interpolate between grid point gradients
    n0: float = dotGridGradient(x0, y0, x, y)
    n1: float = dotGridGradient(x1, y0, x, y)
    ix0: float = interpolate(n0, n1, sx)
    n2: float = dotGridGradient(x0, y1, x, y)
    n3: float = dotGridGradient(x1, y1, x, y)
    ix1: float = interpolate(n2, n3, sx)
    value: float = interpolate(ix0, ix1, sy)
 

    # if draw_cores:
    #     f1,_ = math.modf(x)
    #     f2,_ = math.modf(y)
    #     if f1 < 0.1 and f2 < 0.1 and value < -0.05:
    #         return 42
    #     if f1 < 0.1 and f2 < 0.1:
    #         return 84

    return value
