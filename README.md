Ressources utilisees
---

- https://medium.com/@yvanscher/playing-with-perlin-noise-generating-realistic-archipelagos-b59f004d8401
- https://en.wikipedia.org/wiki/Perlin_noise
- https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/procedural-patterns-noise-part-1/introduction.html (& part 2)
 - https://adrianb.io/2014/08/09/perlinnoise.html
 - https://stackoverflow.com/questions/41168396/how-to-create-a-pygame-surface-from-a-numpy-array-of-float32
 