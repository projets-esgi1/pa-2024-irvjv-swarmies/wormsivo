import os
import time

import pygame
import pygame.freetype

import settings
import game_logic
import physics


wait_for_key = False
paused_time = 0.


def main():
    
    scale = 100.0
    octaves = 6
    persistence = 0.5
    lacunarity = 2.0

    messages = list[game_logic.LogMessage]()

    width, height = settings.dimensions

    worms = game_logic.WormsLogic(messages)
    worms.generate_perlin_world((width, height))

    start_pygame(worms=worms, messages=messages)


def start_pygame(
        worms: game_logic.WormsLogic, 
        messages: list[game_logic.LogMessage]
    ):
    global wait_for_key, paused_time
    start_pause = pygame.time.get_ticks()

    pygame.init()
    width, height = settings.dimensions
    screen = pygame.display.set_mode((width, height))

    
    leve_file_name = os.path.join("data", "level.png")
    level_surface = pygame.image.load(leve_file_name)

    clock = pygame.time.Clock()

    running = True
    game_state = game_logic.states.Start

    font = pygame.freetype.SysFont("serif", 15)

    while running:

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                else:
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_p:
                            if not wait_for_key:
                                start_pause = pygame.time.get_ticks()
                            else:
                                paused_time += pygame.time.get_ticks() - start_pause
                            wait_for_key = not wait_for_key
                        elif event.key == pygame.K_r:
                            for w in worms.selected_worms:
                                w.alive = True
                                w.pos = w.prime_pos
                                w.speed = (0, 0)
                                w.init_speed = w.prime_speed
                                w.init_pos = w.pos
                            worms.physics.start_time = pygame.time.get_ticks() / 1000
                            worms.physics.last_time = worms.physics.start_time

            if wait_for_key:
                continue
            else:
                break

        for w in worms.worms:
            if w.is_mouse_over():
                w.highlighted = True
            else:
                w.highlighted = False


        #screen.fill("purple")
        screen.blit(level_surface, (0, 0))

        if(game_state == game_logic.states.Start):
            worms.draw_start_state(
                screen, 
            )

            for w in worms.worms:
                if w.is_mouse_clicked():
                    worms.selected_worms.append(
                        worms.worms.pop(worms.worms.index(w))
                    )

            if len(worms.selected_worms) >= 2:
                game_state = game_logic.states.Play
                worms.physics = physics.Physics(
                    pygame.time.get_ticks() / 1000, width, height
                )

        elif (game_state == game_logic.states.Play):
            positionsl = worms.compute_physics(paused_time=paused_time)
            for positions in positionsl:
                if len(positions) == 1:
                    pygame.draw.line(
                        screen, game_logic.red, 
                        positions[0], positions[0], width=2
                    )

                elif len(positions) > 1:
                    rect = pygame.draw.lines(
                        screen, game_logic.red, False, 
                        positions, width=2
                    )

                    #print(rect)
                #screen.blit(line_surface, (0, 0))

            worms.draw_play_state(screen)

        ts, rect = font.render(str(clock.get_fps()), size=20)
        screen.blit(ts, (settings.dimensions[0] - rect.width, 5))

        now = time.time()
        for i, mess in enumerate(messages):
            if now - mess.created_at > game_logic.WormsLogger.message_stickiness:
                continue
            ts, rect = font.render(
                str(mess), fgcolor=game_logic.yellow, bgcolor=(0,0,0,), size=20
            )

            screen.blit(
                ts, (
                    settings.dimensions[0] - rect.width, 
                    25 + i * rect.height
                )
            )


        pygame.display.flip()

        clock.tick(settings.fps)

    pygame.quit()

if __name__ == "__main__":
    print ("runpy")
    main()
