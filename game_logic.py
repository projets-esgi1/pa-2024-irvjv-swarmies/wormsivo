import enum
import os.path
import math 
import queue
import threading
import enum 
import typing
import time

import numpy as np
from numpy.typing import NDArray

import pygame

import perlin
import image_printer
import physics
import settings

class states(enum.Enum):
    Start = enum.auto()
    Play = enum.auto()

class LogMessage:
    content: str
    created_at: float

    def __init__(self, content: str) -> None:
        self.content = content
        self.created_at = time.time()

    def __str__(self) -> str:
        return self.content

class WormsLogger:

    message_stickiness = 5000.  #combien de temps un message reste affiché

    q: queue.SimpleQueue[str]
    messages: list[LogMessage]


    def __init__(self, q: queue.SimpleQueue[str], messages: list[LogMessage]) -> None:
        self.q = q
        self.messages = messages

    # apellé automatiquement par threading.Thread.start
    def __call__(self, *args, **kwds) -> typing.Any:
        print("logger thread started")
        while True:
            mess = self.q.get()
            print("got", mess, type(mess))
            self.messages.append(LogMessage(mess))


worm_file_name = os.path.join("assets", "idle_game.png")
worm_surface = pygame.image.load(worm_file_name)
worm_mask = pygame.mask.from_surface(worm_surface)
mask_outline = worm_mask.outline() 

blue = (65,105,225)
green = (34,139,34)
beach = (238, 214, 175)
red = (225, 10, 10)
yellow = (0xFF,0xEB,0x3B)
vide_cutoff = -0.05

def vts(color):
    return {
        blue: "blue",
        green: "green",
        beach: "beach",
        red: "red",
        yellow: "yellow"
    }[color]

def vtc(val):
    if val == 42: return red
    elif val < vide_cutoff: return blue
    elif val < 0: return beach
    else: return green

def add_color(world, width, height, vide_cutoff: float):

    color_world = np.zeros(world.shape+(3,))
    for i in range(height):
        for j in range(width):
            color_world[i][j] = vtc(world[i][j])

    return color_world


class WormsLogic:
    potential_starts: list[tuple[int, int]] = []
    masks: list[pygame.Mask] = []
    world: NDArray[np.float64]
    world_padded: NDArray[np.float64]
    color_world: NDArray[np.float64]
    worms: list["Worm"] = []
    selected_worms: list["Worm"] = []
    physics: physics.Physics
    log: queue.SimpleQueue
    log_thread: threading.Thread
    collisions: list[tuple[int, int]]

    def __init__(self, messages) -> None:
        self.log = queue.SimpleQueue()
        loginst = WormsLogger(self.log,messages)
        self.log_thread = threading.Thread(
            daemon=True, target=loginst, args=(), 
            name="Logging thread"
        )
        
        self.log_thread.start()
        self.collisions = []

    def generate_perlin_world(
            self, dim: tuple[int, int]
    ) -> None:
        
        width, height = dim
        world = np.zeros((height, width))


        for i in range(height):
            for j in range(width):
                world[i, j] = perlin.perlin(i/(height/5), j/(width/6))
        for i in range(height):
            for j in range(width):
                value = perlin.perlin(i/50, j/50)
                world[i, j] = (world[i, j] + value) /2
                f1,_ = math.modf(i/50)
                f2,_ = math.modf(j/50)
                
                if world[i, j] < vide_cutoff and f1 == 0 and f2 == 0:
                    self.potential_starts.append((j, i))
                
                # if world[i, j] < vide_cutoff and f1 < 0.05 and f2 < 0.05:
                #     world[i, j] = 42
        # for i in range(shape[0]):
        #     for j in range(shape[1]):
        #         world[i, j] = (world[i, j] + perlin.perlin(i/25, j/25)) /2
                #world[i][j] = noise.pnoise2(i/scale,j/scale,octaves=octaves, persistence=persistence, lacunarity=lacunarity, repeatx=1024, repeaty=1024, base=0)
        
        self.world = world
        self.world_padded = np.pad(world, (1, 1), constant_values=(1,))

        for ps in self.potential_starts:
            self.worms.append(
                Worm((ps[0],ps[1]), world_dimensions=(width, height), surface=worm_surface)
            )
        
        self.color_world = add_color(
            world, width=width, height=height, vide_cutoff=vide_cutoff
        )

        image = image_printer.toimage(self.color_world)
        #image = PIL.Image.fromarray(world, 'L')
        image.save("data/level.png")
        #image.show()

    def draw_start_state(self, screen: pygame.Surface):
        for worm in self.worms:
            
            pos = get_pixel_coordinates(worm.pos, settings.dimensions)

            screen.blit(
                worm_surface, 
                pos
            )
            
            if worm.highlighted:
                pygame.draw.lines(
                    screen, (255, 0 , 0), True, 
                    list(map(
                        lambda p: (
                            pos[0] + p[0], 
                            pos[1] + p[1]
                        ),
                        worm.mask.outline()
                    )), 1
                )

    def draw_play_state(self, screen: pygame.Surface):
        for worm in self.selected_worms:
            screen.blit(worm_surface, 
                get_pixel_coordinates(worm.pos, settings.dimensions)
            )
        for collision in self.collisions:
            pygame.draw.circle(screen, red, collision, 3)

    def compute_physics(
            self, paused_time: float
    ) -> list[list[tuple[int, int]]]:
        global wait_for_key
        tick = pygame.time.get_ticks() / 1000 - paused_time / 1000
        posis: list[list[tuple[int, int]]] = []
        for w in self.selected_worms:

            if not w.alive:
                continue

            positions: list[tuple[float, float]] = []
            w.pos = self.physics.compute_chute_libre_pos(
                time=tick, 
                pos0=w.init_pos, 
                v0=w.init_speed,
                pos=w.pos,
                points_intermediaires=positions
            )

            if not w.is_in_bounds(self.world):
                self.log.put(
                    f"worm {self.selected_worms.index(w)} died !"
                )
                w.alive = False
                
                continue

            ipositions = list(
                map(
                    lambda p: get_collision_pixel_coordinates(
                        p, settings.dimensions
                    ), 
                    positions
                )
            )

            for x,y in ipositions:
                print("xy", x, y, tuple(self.color_world[y, x]))
                if tuple(self.color_world[y, x]) != blue:
                    
                    print("collision", x, y, tuple(self.color_world[y, x]))
                    
                    self.collisions.append((x, y))
                    w.speed = self.physics.compute_chute_libre_vit(
                        time=tick, v0=w.init_speed
                    )

                    bounciness = self.get_9_bounce_values(x, y)

                    print(
                        "bounciness", 
                        list(map(lambda x: (x, vts(vtc(x))), bounciness))
                    )
                    print("bounciness", bounciness)
                    print("normal", self.get_normal(x, y))
                    #a = input()
                    n = self.get_normal(x, y)
                    #w.init_speed = n
                    w.init_speed = w.speed[0] * n[0], w.speed[1]*n[1]
                    w.init_pos = w.pos
                    w.speed = (0, 0)
                    break


            print("points inter", positions, "pixel", ipositions)

            posis.append(ipositions)



            #print("bounce", str(w.pos), bounciness)

        return posis

    def get_9_bounce_values(self, x, y):
        x = x+1
        y= y+1
        return list(self.world_padded[y-1, x-1:x+2]) \
            + list(self.world_padded[y, x-1:x+2]) \
            + list(self.world_padded[y+1, x-1:x+2])
    
    def get_normal(self, x, y):
        finy = 0
        finx = 0

        for iy in range(-1, 2):
            for ix in range(-1, 2):
                mul = 0
                raw = self.world_padded[y + iy+1, x + ix+1]
                col = vtc(raw)
                print(raw, vts(col))
                if col == beach:
                    mul = 0.1
                elif col == green:
                    mul = 0.6
                finy += iy * mul 
                finx += ix * mul 

        # la normale va vers l'opposé
        # les y sont deja inversés
        return -finx, finy


class Worm:
    pos: tuple[float, float]
    speed: tuple[float, float]
    init_pos: tuple[float, float]
    init_speed: tuple[float, float]

    prime_pos: tuple[float, float]
    prime_speed: tuple[float, float]

    mask: pygame.mask.Mask
    rect: pygame.Rect
    alive: bool
    world_dimensions: tuple[int, int]

    surface: pygame.Surface

    highlighted: bool

    def __init__(
        self, 
        pos: tuple[float, float], 
        world_dimensions: tuple[int, int],
        surface: pygame.Surface
    ) -> None:
        pixelx, pixely = pos
        self.init_pos = (
            pixelx / physics.Physics.pixels_par_metre,
            ((world_dimensions[1] - pixely) / physics.Physics.pixels_par_metre)
        )
        self.pos = self.init_pos
        self.init_speed = (20, 15)

        self.prime_pos = self.pos
        self.prime_speed = self.init_speed

        self.world_dimensions = world_dimensions
        self.surface = surface

        self.mask = pygame.mask.from_surface(worm_surface)
        self.rect = pygame.Rect(
            pos[0]-(worm_surface.get_width() / 2), 
            pos[1]-(worm_surface.get_height() / 2), 
            worm_surface.get_width(), worm_surface.get_height())
        self.alive = True

    def is_mouse_over(self):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        rel_x, rel_y = mouse_x - self.rect.left, mouse_y - self.rect.top
        return self.rect.collidepoint(
            mouse_x, mouse_y
        ) and self.mask.get_at((rel_x, rel_y))  
        # Check if mouse overlaps with the sprite's mask

    def is_mouse_clicked(self):
        left, _, _ = pygame.mouse.get_pressed()
        return self.is_mouse_over() and left

    def is_in_bounds(self, world: np.ndarray):
        wh, ww = world.shape
        
        wx, wy = self.pos
        return wx >= 0 and wy >= 0 and wx <= ww and wy <= wh
    
    # def get_pixel_coordinates(self):
    #     width, height = self.world_dimensions

    #     pos = (
    #         self.pos[0] * physics.Physics.pixels_par_metre, 
    #         height - (self.pos[1] * physics.Physics.pixels_par_metre)
    #     )

    #     pos = (
    #         pos[0] +(worm_surface.get_width() / 2), 
    #         (pos[1])
    #     )

    #     return pos


def get_pixel_coordinates(pos, world_dimensions):
    width, height = world_dimensions

    pos = (
        pos[0] * physics.Physics.pixels_par_metre, 
        height - (pos[1] * physics.Physics.pixels_par_metre)
    )

    pos = (
            pos[0] -(worm_surface.get_width() / 2), 
            (pos[1] -(worm_surface.get_height() / 2))
            # int(pos[0]), 
            # int(pos[1] + worm_surface.get_height() / 2)
    )

    return pos

def get_collision_pixel_coordinates(pos, world_dimensions):
    width, height = world_dimensions

    pos = (
        pos[0] * physics.Physics.pixels_par_metre, 
        height - (pos[1] * physics.Physics.pixels_par_metre)
    )

    pos = (
            int(pos[0]), 
            int(pos[1] + worm_surface.get_height() / 2)
    )

    return pos
