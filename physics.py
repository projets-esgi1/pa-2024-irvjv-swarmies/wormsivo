import numpy as np


GRAVITY = -9.81
GRAV_POS = GRAVITY/2

class Physics:
    last_time: float
    start_time: float
    objects: list[tuple[float, float]]
    game_width: float
    game_height: float
    pixels_par_metre: int = 10

    def __init__(self, start_time, game_width, game_height) -> None:
        self.start_time = start_time
        self.game_width = game_width
        self.game_height = game_height
        self.last_time = start_time

    def add_object(self, obj: tuple[float, float]):
        self.objects.append(obj)

    def compute_chute_libre_pos(
        self, time: float, 
        pos0: tuple[float, float],
        v0: tuple[float, float],
        pos: tuple[float, float],
        points_intermediaires: list[tuple[float, float]]
    ) -> tuple[float, float]:
        t = time - self.start_time
        vx0, vy0 = v0
        x0, y0 = pos0

        print("pos depart", pos)
        
        # on trouve le couplet x,y de la position
        # d'arrivée pour un temps t
        pos_arrivee =  (x0 + vx0*t), \
            (GRAV_POS * (t**2) + vy0 * t + y0)

        # on recherche tous les t qui correspondent a
        # un pixel du terrain vu que nos collisions
        # se font au pixel entre nos 2 t
        xarr, yarr = pos_arrivee
        xin, yin = pos

        #ydel = yarr - yin
        #ydelpix = ydel * Physics.pixels_par_metre

        #xdel = xarr - xin
        #xdelpix = xdel * Physics.pixels_par_metre

        #delta = ydelpix if abs(ydelpix) > abs(xdelpix) else xdelpix
        #diff = ydel if abs(ydel) > abs(xdel) else xdel

        #tstep = abs(diff / delta)

        #print("BIGGEST DELTA", tstep, xdelpix, ydelpix)

        # for x in range(
        #     int(xin*Physics.pixels_par_metre), 
        #     int(xarr*Physics.pixels_par_metre)
        #     , max(1,int(tstep* Physics.pixels_par_metre))
        # ):
        #     print("x", x, "(", x/Physics.pixels_par_metre,")")

        #     px = x / Physics.pixels_par_metre
        #     tp = (px - x0) / vx0
        #     py = (GRAV_POS * (tp**2) + vy0 * tp + y0)
        #     y = int(py)
        #     points_intermediaires.append((px, py))
        for tm in timerange(xin, xarr, yin, yarr, self.last_time, time):
            ti = tm - self.start_time
            xm = (x0 + vx0*ti)
            ym = (GRAV_POS * (ti**2) + vy0 * ti + y0)
            points_intermediaires.append((xm, ym))


        print("pos arrivee", pos_arrivee)

        self.last_time = time
        return pos_arrivee

    def compute_chute_libre_vit(
        self, 
        time: float, 
        v0: tuple[float, float]
    ):
        vx0, vy0 = v0
        t = time - self.start_time
        
        return vx0, GRAVITY * t + vy0
    
def timerange(xin, xarr, yin, yarr, tin, tout):
    xd = xarr - xin
    yd = yarr - yin
    d = xd if abs(xd) > abs(yd) else yd

    print("tin tout steps", tin, tout, abs(int(d * Physics.pixels_par_metre)))
    for t in np.linspace(tin, tout, num=abs(int(d * Physics.pixels_par_metre))):
        yield t
